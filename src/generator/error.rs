use std::error;
use std::fmt;
use std::num;
use std::result;

pub type Result<T> = result::Result<T, ParseGeneratorError>;

#[derive(Debug)]
pub enum ParseGeneratorError {
    /// Generator expression was empty.
    Empty,

    /// Encountered an unexpected segment.
    UnexpectedSegment,

    /// Encountered an error when parsing a segment of a generator expression.
    ///
    /// In practice, this will be the error of a call to `parse()` by our
    /// parse operation.
    Other(Box<error::Error>),
}

impl fmt::Display for ParseGeneratorError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ParseGeneratorError::*;
        match self {
            Empty => write!(f, "Generator expression was empty"),
            UnexpectedSegment => write!(f, "Encountered an unexpected segment while parsing"),
            Other(_) => write!(f, "Error while parsing expression segment"),
        }
    }
}

impl error::Error for ParseGeneratorError {
    fn cause(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            ParseGeneratorError::Other(e) => Some(e.as_ref()),
            _ => None,
        }
    }
}

impl From<num::ParseIntError> for ParseGeneratorError {
    fn from(e: num::ParseIntError) -> Self {
        ParseGeneratorError::Other(Box::new(e))
    }
}
