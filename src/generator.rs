mod error;

use crate::generator::ParseGeneratorError;
use rand::{distributions::Uniform, Rng};
use std::fmt::{self, Display};
use std::str::FromStr;

pub use self::error::*;

pub trait GeneratorIter: Iterator<Item = DiceGenerator> {}

impl<T: Iterator<Item = DiceGenerator>> GeneratorIter for T {}

pub trait Generator {
    type Output: Display;

    /// Realizes the generator's output.
    fn roll(&self, rng: &mut impl Rng) -> Self::Output;
}

/// A generator representing the throw of a single size of dice.
///
/// Conceptually, `DiceGenerator` maps to throwing `n` dice of a size `m`.
pub struct DiceGenerator {
    count: u32,
    range: Uniform<u32>,
}

impl DiceGenerator {
    pub fn new(range: u32) -> Self {
        DiceGenerator {
            count: 1,
            range: (0..range).into(),
        }
    }
}

impl FromStr for DiceGenerator {
    type Err = ParseGeneratorError;

    fn from_str(s: &str) -> Result<Self> {
        let mut s = s.split('d');
        let lhs = s.next().ok_or(ParseGeneratorError::Empty)?.parse()?;
        let rhs = s.next().map(|rhs| rhs.parse());

        // Valid generators have one or two segments, but never three.
        if s.next().is_some() {
            return Err(ParseGeneratorError::UnexpectedSegment);
        }

        Ok(match rhs {
            None => DiceGenerator {
                count: 1,
                range: (0..lhs).into(),
            },

            Some(rhs) => DiceGenerator {
                count: lhs,
                range: (0..rhs?).into(),
            },
        })
    }
}

pub struct DiceGeneratorOutput {
    sum: u32,
    values: Vec<u32>,
}

impl Display for DiceGeneratorOutput {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.values.split_last() {
            None => write!(f, "{}", self.sum),
            Some((tail, body)) => {
                write!(f, "{} [", self.sum)?;
                for item in body {
                    write!(f, "{}, ", item)?;
                }
                write!(f, "{}]", tail)
            }
        }
    }
}

impl Generator for DiceGenerator {
    type Output = DiceGeneratorOutput;

    fn roll(&self, rng: &mut impl Rng) -> Self::Output {
        let mut state = (0, Vec::new());
        for _ in 0..self.count {
            let next = rng.sample(&self.range) + 1;
            state.0 += next;
            state.1.push(next);
        }

        let (sum, values) = state;
        DiceGeneratorOutput { sum, values }
    }
}
