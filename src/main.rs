mod generator;

use crate::generator::*;
use rand::{rngs::OsRng, RngCore};
use std::iter;

fn main() {
    let mut rng = get_rng();

    for generator in generators() {
        println!("{}", generator.roll(&mut rng));
    }
}

fn generators() -> impl GeneratorIter {
    let mut generators = std::env::args().skip(1).filter_map(|s| s.parse().ok());
    let first = generators.next().unwrap_or_else(|| DiceGenerator::new(6));

    iter::once(first).chain(generators)
}

fn get_rng() -> Box<dyn RngCore> {
    OsRng::new()
        .map(|x| Box::new(x) as Box<RngCore>)
        .unwrap_or_else(|_| Box::new(rand::thread_rng()) as Box<RngCore>)
}
